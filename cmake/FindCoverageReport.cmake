find_program( GCOV_PATH gcov )
find_program( LCOV_PATH lcov )
find_program( GENHTML_PATH genhtml )
if(NOT GCOV_PATH)
    message(FATAL_ERROR "gcov not found! Aborting...")
endif() # NOT GCOV_PATH
if(NOT LCOV_PATH)
    message(FATAL_ERROR "lcov not found! Aborting...")
endif() # NOT LCOV_PATH
if(NOT GENHTML_PATH)
    message(FATAL_ERROR "genhtml not found! Aborting...")
endif() # NOT GENHTML_PATH

add_custom_target(
        coveragereport
        
        COMMENT "Processing code coverage counters and generating report."

        COMMAND rm -rf coverage
        COMMAND ${CMAKE_COMMAND} -E remove coverage-base.info
        COMMAND ${CMAKE_COMMAND} -E remove coverage-test.info
        COMMAND ${CMAKE_COMMAND} -E remove coverage-total.info
        COMMAND ${CMAKE_COMMAND} -E remove coverage_a.info
        COMMAND ${CMAKE_COMMAND} -E remove coverage.info
        

        # Initial Baseline
   
        COMMAND ${LCOV_PATH} -q --base-directory ${CMAKE_SOURCE_DIR}/src  --directory . --no-external --capture --initial  --output-file coverage-base.info

        # Run tests
        COMMAND testexecutable

        # Capturing lcov counters and generating report
        COMMAND ${LCOV_PATH} -q --base-directory ${CMAKE_SOURCE_DIR}/src  --directory . --no-external --capture --output-file coverage-test.info

        # Combine 
        COMMAND ${LCOV_PATH}  -q --add-tracefile coverage-base.info --add-tracefile coverage-test.info --output-file coverage-total.info
        
        # Remove any sources asked to
        COMMAND ${LCOV_PATH}  -q --base-directory ${CMAKE_SOURCE_DIR}/src --directory . --no-external --remove coverage-total.info "${CMAKE_SOURCE_DIR}/tests/*" --output-file coverage_a.info
        COMMAND ${LCOV_PATH} -q --base-directory ${CMAKE_SOURCE_DIR}/src --directory . --no-external --remove coverage_a.info ${COVERAGE_EXCLUDE_SOURCES} --output-file coverage.info

        COMMAND ${LCOV_PATH} --list coverage.info 

        # Generate report
        COMMAND ${GENHTML_PATH} -o coverage coverage.info

        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    
)


find_program(CPPCHECK cppcheck)
if(!CPPCHECK_BIN)
    message("cppcheck not found and is required to build")
endif()


add_custom_target(analysis
                    COMMENT "cppcheck... with ignores '${CPPCHECK_IGNORES}'"
                    COMMAND bash -c "cppcheck -q --force --error-exitcode=1 --enable=all --std=c99 -i build/ ${CPPCHECK_IGNORES} ../"
                    )
